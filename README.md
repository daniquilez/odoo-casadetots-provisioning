# Ansible scripts to provision and deploy Odoo

These are [Ansible](http://docs.ansible.com/ansible/) playbooks (scripts) for managing an [Odoo](https://github.com/odoo/odoo) server.

## Requirements

You will need Ansible on your machine to run the playbooks.
These playbooks will install the PostgreSQL database, NodeJS and Python virtualenv to manage python packages.

It has currently been tested on **Ubuntu 16.04 Xenial (64 bit)**.

1. Clone this repo and [odoo-provisioning](https://gitlab.com/femprocomuns/odoo-provisioning) in the same directory
2. Go to `odoo-provisioning` directory and install Ansible dependencies:
   ```
   ansible-galaxy install -r requirements.yml
   ```

## System administration

The [sys_admins](https://gitlab.com/femprocomuns/odoo-provisioning/blob/master/playbooks/sys_admins.yml) playbook uses the community role [sys-admins-role](https://github.com/coopdevs/sys-admins-role).

Define the list of users that you want to be created as system administrators.

We use `host_vars` to declare per environment variables:
```yaml
# inventory/host_vars/<YOUR_HOST>/config.yml

system_administrators:
  - name: pepe
    ssh_key: "{{ inventory_dir }}/../pub_keys/pepe.pub"
    state: present
    - name: paco
    ssh_key: "{{ inventory_dir }}/../pub_keys/paco.pub"
    state: present
```

The first time you run it against a brand new host you need to run it as `root` user.
You'll also need passwordless SSH access to the `root` user.
```
ansible-playbook playbooks/sys_admins.yml --limit=<environment_name> -u root
```

For the following executions, the script will asssume that your user is included in the system administrators list for the given host.

To run the playbook as a system administrator just use the following command:
```
ansible-playbook playbooks/sys_admins.yml --limit=dev
```

Ansible will try to connect to the host using the system user. If your user as a system administrator is different than your local system user please run this playbook with the correct user using the `-u` flag.
```
ansible-playbook playbooks/sys_admins.yml --limit=dev -u <username>
```

## Provision
Go to `odoo-provisioning` directory and run `ansible-playbook` command pointing to the `inventory/hosts` file of this repository:
```
ansible-playbook playbooks/provision.yml -i ../odoo-casadetots-provisioning/inventory/hosts --ask-vault-pass --limit=prod
```
